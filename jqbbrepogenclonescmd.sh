#!/bin/bash

# JQBBREPOGENCLONECMD

# Take list of repositories from Workspace and generate clone commands for all of them
#
# Usage:
#
#  1) Use "jqbbget" to get JSON stream for repository list from
#         https://api.bitbucket.org/2.0/repositories/{workspace}
#     MUST USE "jqbbget to pull repository list, not use "paged" output directly from BB API
#
#     Example:
#         bash$ jqbbget https://api.bitbucket.org/2.0/repositories/xyzzy > xyzzyrepos.json
#
#  2) Run this program with name of JSON file from (1), output to either file or input to bash
#
#     Example:
#         bash$ jqbbrepogenclonecmd xyzzyrepos.json > cmd
#
#       <inspect file "cmd" for correctness>
#
#         bash$ bash cmd
#
#     Example:
#         bash$ jqbbrepogenclonecmd xyzzyrepos.json | bash
#
#  Results:
#     Every repository in workspace (i.e. "xyzzy") will be cloned, into a subdirectory of 
#     "xyzzy" (i.e. xyzzy/repo1/repo1-HG, xyzzy/repo2/repo2-HG, etc)

jq -r '
if ((type == "array") and ((.[0]|type) == "object") and ((.[0].type) == "repository")) then
  "showresult() {
      msg=$1; shift;
      \"$@\" && echo \"$msg\" SUCCESS 1>&2 || echo \"$msg\" FAIL 1>&2
  }
  ", (.[] |
  "reponame=\(.full_name)
   repoBname=$(basename ${reponame})
   showresult \"${reponame}: mkdir\" mkdir -p \"${reponame}\"
   showresult \"${reponame}: clone\" hg clone \(.links.clone|map({"key":.name,"value":.href})|from_entries|.https) \"${reponame}/${repoBname}-HG\" 
   ")
else
  "Input is not a repository JSON stream\n" | halt_error(1)
end
' "$@"
