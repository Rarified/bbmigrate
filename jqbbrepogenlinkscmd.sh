#!/bin/bash

# JQBBREPOGENLINKSCMD

# Take list of repositories from Workspace and retrieve metadata for all of them
#
# Usage:
#
#  1) Use "jqbbget" to get JSON stream for repository list from
#         https://api.bitbucket.org/2.0/repositories/{workspace}
#     MUST USE "jqbbget" to pull repository list, not use "paged" output directly from BB API
#
#     Example:
#         bash$ jqbbget https://api.bitbucket.org/2.0/repositories/xyzzy > xyzzyrepos.json
#
#  2) Run this program with name of JSON file from (1), output to either file or input to bash
#
#     Example:
#         bash$ jqbbrepogenlinkscmd xyzzyrepos.json > cmd
#
#       <inspect file "cmd" for correctness>
#
#         bash$ bash cmd
#
#     Example:
#         bash$ jqbbrepogenlinkscmd xyzzyrepos.json | bash
#
#  Results:
#     Metadata for each repository in workspace (i.e. "xyzzy" in examples) will be stored in
#     JSON streams into files in each repositorie's subdirectory, with names corresponding
#     to class of metadata, i.e.
#
#     For repository "xyzzy/repo1", the following would be created:
#        xyzzy/repo1/pullrequests-DECLINED.json
#        xyzzy/repo1/pullrequests-MERGED.json
#        xyzzy/repo1/pullrequests-OPEN.json
#        xyzzy/repo1/pullrequests-SUPERSEDED.json
#        xyzzy/repo1/commits.json
#        xyzzy/repo1/watchers.json
#        xyzzy/repo1/hooks.json
#        xyzzy/repo1/forks.json

# Additional metadata categories can be added with "jqbbget" commands as below

jq -r '
if ((type == "array") and ((.[0]|type) == "object") and ((.[0].type) == "repository")) then
  "showresult() {
      msg=$1; shift;
      \"$@\" && echo \"$msg\" SUCCESS 1>&2 || echo \"$msg\" FAIL 1>&2
  }",
  ( .[] |
  "fullname=\(.full_name)
  showresult \"${fullname}: mkdir\" mkdir -p ${fullname}
  ( cd ${fullname}
      for state in MERGED SUPERSEDED OPEN DECLINED; do
          showresult \"${fullname}: pullrequests-$state\" jqbbget \(.links.pullrequests.href)\\?state=$state > pullrequests-$state.json
      done
      showresult \"${fullname}: commits\" jqbbget \(.links.commits.href) > commits.json
      showresult \"${fullname}: watchers\" jqbbget \(.links.watchers.href) > watchers.json
      showresult \"${fullname}: hooks\" jqbbget \(.links.hooks.href) > hooks.json
      showresult \"${fullname}: forks\" jqbbget \(.links.forks.href) > forks.json
   )" )
else
   "Input is not a repository JSON stream\n" | halt_error(1)
end
' "$@"
