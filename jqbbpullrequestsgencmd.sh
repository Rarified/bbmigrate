#!/bin/bash

# JQBBPULLREQUESTGENCMD

# Take pullrequest metadata for a repository and retrieve full metadata (and source repository
#    clones) for all pull requests
#
# Usage:
#
#  1) Use "jqbbrepogenlinkscmd" to get JSON streams for repository metadata from
#         https://api.bitbucket.org/2.0/repositories/{workspace}/{repo_slug}
#         See: jqbbrepogenlinkscmd source for Usage
#
#     Example:
#         bash$ jqbbrepogenlinkscmd xyzzyrepos.json | bash
#
#  2) Run this program, from inside a repository subdirectory, with name of the pulldata JSON file(s) from (1),
#         output to either file or input to bash
#
#     Example:
#         bash$ cd xyzzy/repo1
#         bash$ jqbbpullrequestsgencmd pullrequests-OPEN.json > cmd
#
#       <inspect file "cmd" for correctness>
#
#         bash$ bash cmd
#
#     Example:
#         bash$ cd xyzzy/repo1
#         bash$ jqbbpullrequestsgencmd pullrequests-OPEN.json | bash
#
#  Results:
#     Pull request metadata and source repositories will be retrieved into "pullrequests"
#     and "pullrequests/src_repos" subdirectories respectively.  Sample structure:
#
#        -rw-r--r--    142015 Mar 29 14:43 ./xyzzy/repo1/commits.json
#        -rw-r--r--     37446 Mar 29 14:43 ./xyzzy/repo1/forks.json
#        -rw-r--r--         5 Mar 29 14:43 ./xyzzy/repo1/hooks.json
#        -rw-r--r--    103674 Mar 29 14:43 ./xyzzy/repo1/pullrequests-DECLINED.json
#        -rw-r--r--    252440 Mar 29 14:43 ./xyzzy/repo1/pullrequests-MERGED.json
#        -rw-r--r--     27482 Mar 29 14:43 ./xyzzy/repo1/pullrequests-OPEN.json
#        -rw-r--r--         3 Mar 29 14:43 ./xyzzy/repo1/pullrequests-SUPERSEDED.json
#        -rw-r--r--     40818 Mar 29 15:56 ./xyzzy/repo1/pullrequests/1/activity.json
#        -rw-r--r--     24520 Mar 29 15:56 ./xyzzy/repo1/pullrequests/1/comments.json
#        -rw-r--r--      8482 Mar 29 15:56 ./xyzzy/repo1/pullrequests/1/commits.json
#        -rw-r--r--         5 Mar 29 15:56 ./xyzzy/repo1/pullrequests/1/status.json
#        -rw-r--r--     21714 Mar 29 15:57 ./xyzzy/repo1/pullrequests/2/activity.json
#        -rw-r--r--     10661 Mar 29 15:57 ./xyzzy/repo1/pullrequests/2/comments.json
#        -rw-r--r--     13304 Mar 29 15:57 ./xyzzy/repo1/pullrequests/2/commits.json
#        -rw-r--r--         5 Mar 29 15:57 ./xyzzy/repo1/pullrequests/2/status.json
#        drwxr-xr-x         9 Mar 29 15:47 ./xyzzy/repo1/pullrequests/src_repos/repo1-Fork1/repo1-HG
#        drwxr-xr-x         9 Mar 29 15:48 ./xyzzy/repo1/pullrequests/src_repos/repo1-Fork2/repo1-HG
#        drwxr-xr-x         9 Mar 29 14:43 ./xyzzy/repo1/repo1-HG
#        -rw-r--r--     11200 Mar 29 14:43 ./xyzzy/repo1/watchers.json

jq -r '
if ((type == "array") and ((.[0]|type) == "object") and ((.[0].type) == "pullrequest")) then
  "showresult() {
      msg=$1; shift;
      \"$@\" && echo \"${msg}\" SUCCESS 1>&2 || echo \"${msg}\" FAIL 1>&2
   }
  ", (.[] |
       "echo ==== Pull request \(.id);
        mkdir -p pullrequests/\(.id);
        file=pullrequests/\(.id)/activity.json; showresult \"$file:\" jqbbget \(.links.activity.href) > $file
        file=pullrequests/\(.id)/comments.json; showresult \"$file:\" jqbbget \(.links.comments.href) > $file
        file=pullrequests/\(.id)/commits.json; showresult \"$file:\" jqbbget \(.links.commits.href) > $file
        file=pullrequests/\(.id)/statuses.json; showresult \"$file:\" jqbbget \(.links.statuses.href) > $file
        reponame=\(.source.repository.full_name)
        if [ -d pullrequests/src_repos/${reponame}-HG ] ; then
            showresult \"${reponame}: pull\" hg pull -u -R pullrequests/src_repos/${reponame}-HG;
        else
            showresult \"${reponame}: clone\" hg clone \(.source.repository.links.html.href) pullrequests/src_repos/${reponame}-HG;
        fi")
else
  "Input is not a pullrequest JSON stream\n" | halt_error(1)
end
' "$@"
